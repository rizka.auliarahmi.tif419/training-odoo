# -*- coding: utf-8 -*-
{
    'name': "Employee",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,
    'author': "PT. Neural Technologies Indonesia",
    'website': "http://nti.co.id",
    'category': 'Uncategorized',
    'version': '14.0.20220714',

    # Tambahkan depend ke module 'base' dan 'hr'
    'depends': ['base','hr'],

    # Daftarkan file view & security
    'data': [
        'security/ir.model.access.csv',
        'views/hr_certificate_views.xml',
        'views/hr_employee_views.xml',
    ],
    'demo': [],
    'application': True
}
