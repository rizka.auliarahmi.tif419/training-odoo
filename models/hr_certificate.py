from select import select
from unittest import defaultTestLoader
from odoo import models, fields, api

class HRCertificate(models.Model):
    _name = 'hr.certificate'

    # Tambahkan field berikut :
    # Name - String
    name = fields.Char(string='Name')

    # Type - Selection dengan element (Seminar,Training,Competition,Appreciation)
    type = fields.Selection(
        [("seminar","Seminar"),
        ("training","Training"),
        ("competition","Competition"),
        ("appreciation","Appreciation")
    ], default="seminar", string='Type')
    

    # Attachment - M2M (Many-to-Many) terhadap model 'ir.attachment'
    attachment = fields.Many2many(
        comodel_name='ir.attachment', 
        string='Attachment'
    )
    
    # Issued Date - Date
    date_of_issue = fields.Date(string='Date of Issue')

    # Employee - Many-to-One terhadap hr.employee
    employee_id = fields.Many2one('hr.employee', string='Employee',)

    